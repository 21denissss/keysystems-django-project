from .models import *


def get_statistics(request):
    # Генерация "количеств" некоторых главных объектов
    num_books = Book.objects.all().count()
    num_instances = BookInstance.objects.all().count()

    # Доступные книги (статус = 'a')
    num_instances_available = BookInstance.objects.filter(status__icontains='a').count()
    num_authors = Author.objects.count()  # Метод 'all()' применен по умолчанию.

    num_visits = request.session.get('num_visits', 1)
    request.session['num_visits'] = num_visits + 1

    # Количество жанров
    num_genres = Genre.objects.count()

    return {
               'num_books': num_books,
               'num_instances': num_instances,
               'num_instances_available': num_instances_available,
               'num_authors': num_authors,
               'num_genres': num_genres,
                'num_visits': num_visits,
           }
